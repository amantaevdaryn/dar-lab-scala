FROM darecosystem/alpine-jdk8:latest

MAINTAINER Daryn

ENV APP_NAME dar_scala-0.1.zip
ENV APP_DIR app
ENV RUN_SCRIPT dar_scala

RUN mkdir -p /root/config/ \
      && apk add --no-cache bash

COPY ./src/main/resources/*.conf /root/config/

WORKDIR /root
COPY ./target/universal/$APP_NAME /root/
RUN unzip -q $APP_NAME
WORKDIR /root/$APP_DIR/bin

RUN rm /root/$APP_NAME
CMD chmod +x $RUN_SCRIPT

CMD ["/bin/bash", "-c", "./$RUN_SCRIPT -Dconfig.file=/root/config/application.conf"]
