package kz.dar.lab

abstract class Animal(animalClass: String)

abstract class Domestic(animalClass: String, name: String, ownerName: String) extends Animal(animalClass)

abstract class Wild(animalClass: String) extends Animal(animalClass)


case class Dog(animalClass: String, name: String, ownerName: String) extends Domestic(animalClass, name, ownerName)

case class Cat(animalClass: String, name: String, ownerName: String) extends Domestic(animalClass, name, ownerName)

case class Fox(animalClass: String) extends Wild(animalClass)

object Main {

  def check(animal: Animal): String = {
    if (animal.isInstanceOf[Domestic]) {
      animal match {
        case Dog(animalClass, name, ownerName) =>
          s"This is a dog with class $animalClass and name $name and owner $ownerName"
        case Cat(animalClass, name, ownerName) =>
          s"This is a cat with class $animalClass and name $name and owner $ownerName"
        case _ =>
          s"Neither dog or cat"
      }
    }
    else "Some wild animal"
  }

  def main(args: Array[String]) = {
    val cat = Cat("mammal", "Barsik", "Daryn");
    val dog = Dog("mammal", "Chappi", "Daryn");
    val fox = Fox("mammal");
    println(check(cat));
    println(check(dog));
    println(check(fox));
  }
}
