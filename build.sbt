name := "dar_scala"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaVersion = "2.6.8"
val AkkaHttpVersion = "10.2.0"
val alpakkaVersion = "1.1.2"
val json4sVersion = "3.6.7"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed"     % AkkaVersion,
  "org.scalatest"     % "scalatest_2.13"        % "3.2.0" % "test",
  "com.typesafe.akka" %% "akka-slf4j"           % AkkaVersion,
  "ch.qos.logback"    % "logback-classic"       % "1.2.3",
  "io.zeebe"          % "zeebe-client-java"     % "0.24.1",
  "com.typesafe.akka" %% "akka-stream"          % AkkaVersion,
  "com.typesafe.akka" %% "akka-http"            % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "ch.megard"         %% "akka-http-cors"       % "0.4.1",
  "io.circe"          %% "circe-core"           % "0.13.0",
  "io.circe"          %% "circe-generic"        % "0.13.0",
  "io.circe"          %% "circe-parser"         % "0.13.0",
  "io.circe"          %% "circe-literal"        % "0.13.0",
  "io.circe"          %% "circe-generic-extras" % "0.13.0",
  "io.circe"          %% "circe-jawn"           % "0.13.0",
  "joda-time"         % "joda-time"             % "2.10.5",
  "org.joda"          % "joda-convert"          % "2.2.1",
  "io.circe"          %% "circe-bson"           % "0.4.0",
  
  "org.slf4j"                  % "slf4j-api"          % "1.7.5",
  "org.slf4j"                  % "slf4j-simple"       % "1.7.5",

  "org.postgresql"            % "postgresql"      % "42.2.6",
  "com.typesafe.slick"        %% "slick"          % "3.3.2",
  "com.typesafe.slick"        %% "slick-hikaricp" % "3.3.2",

  "com.lightbend.akka"         %% "akka-stream-alpakka-amqp"   % alpakkaVersion,
  "org.json4s"                 %% "json4s-jackson"             % json4sVersion,
  "org.json4s"                 %% "json4s-native"              % json4sVersion,
  "com.typesafe.akka"          %% "akka-stream-kafka"          % "2.0.1"




)
enablePlugins(JavaAppPackaging)
