package kz.dar.lab.services

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, Uri}
import kz.dar.lab.model.UserRequest

import io.circe.generic.auto._
import io.circe.syntax._

import scala.concurrent.{ExecutionContext, Future}

case class ClientService()(implicit val system: ActorSystem[_], executionContext: ExecutionContext) {
  def getUserRequest(idNumber: String): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"http://localhost:8080/user/$idNumber")
    ))
  }

  def postUserRequest(userRequest: UserRequest): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(
      method = HttpMethods.POST,
      uri = Uri("http://localhost:8080/user/"),
      entity = HttpEntity(ContentTypes.`application/json`, userRequest.asJson.noSpaces)
    ))
  }

  def putUserRequest(userRequest: UserRequest): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(
      method = HttpMethods.PUT,
      uri = Uri("http://localhost:8080/user/"),
      entity = HttpEntity(ContentTypes.`application/json`, userRequest.asJson.noSpaces)
    ))
  }

  def deleteUserRequest(idNumber: String): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(
      method = HttpMethods.DELETE,
      uri = Uri(s"http://localhost:8080/user/$idNumber"),
    ))
  }
}
