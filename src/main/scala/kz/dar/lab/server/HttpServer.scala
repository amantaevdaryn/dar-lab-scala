package kz.dar.lab.server

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.typesafe.config.Config
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import kz.dar.lab.routes.{WorkflowRoute, WorkflowRouteWrapper}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}
import scala.concurrent.duration._


class HttpServer(route: Route, system: ActorSystem[_], config: Config) extends RouteConcatenation {

  implicit val classicActorSystemProvider: akka.actor.ActorSystem = system.classicSystem
  private val shutdown = CoordinatedShutdown(system)
  implicit val executionContext: ExecutionContext = system.executionContext

  def start(): Unit = {
    val mainRoute = cors()(
      concat(route)
    )
    Http().newServerAt(config.getString("app.host"), config.getInt("app.port")).bind(mainRoute).onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info("DAR-API online at http://{}:{}/", address.getHostString, address.getPort)

        shutdown.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "http-graceful-terminate") { () =>
          binding.terminate(10.seconds).map { _ =>
            system.log
              .info("DAR-api http://{}:{}/ graceful shutdown completed", address.getHostString, address.getPort)
            Done
          }
        }
      case Failure(exception) =>
        system.log.error("Failed to bind HTTP endpoint, terminating system", exception)
        system.terminate()
    }
  }

}
