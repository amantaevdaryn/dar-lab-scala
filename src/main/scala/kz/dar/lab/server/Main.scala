package kz.dar.lab.server

import akka.actor.Props
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.server.Route
import com.typesafe.config.{Config, ConfigFactory}
import kz.dar.lab.actors.{UserRequestActor, WorkflowActor}
import kz.dar.lab.routes.{UserRequestRouteWrapper, WorkflowRouteWrapper}
import akka.http.scaladsl.server.Directives.concat
import akka.stream.alpakka.amqp.AmqpConnectionProvider
import kz.dar.lab.rabbit.{RabbitMQCommandConsumer, RabbitMQConnector, RabbitMQManager}
import kz.dar.lab.repository.{RequestPostgresImpl, RequestPostgresRepo, UserPostgresImpl, UserPostgresRepo}
import kz.dar.lab.repository.{RequestPostgresRepo, UserPostgresImpl, UserPostgresRepo}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.duration._
import scala.util.{Failure, Success}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import kz.dar.lab.kafka.{KConsumer, KConsumerMessage, KProduceMessage, KProducer, KafkaConsumerCommand, KafkaProducerCommand}

import scala.language.postfixOps


object Main extends RabbitMQConnector {
  def startHttpServer(route: Route, system: ActorSystem[_], config: Config): Unit = {
    new HttpServer(route, system, config).start()
  }

  def main(args: Array[String]): Unit = {
    val rootBehavior = Behaviors.setup[Nothing] { context =>
      val config = ConfigFactory.load()
      implicit val executionContext = context.system.executionContext
      implicit val system: ActorSystem[_] = context.system
      implicit val amqpConnectionProvider: AmqpConnectionProvider = createAmqpConnection(config)

      implicit val timeout = Timeout(5 seconds)


      val userDb = Database.forConfig("dbPostgres.users")

      val usersPostgresRepo = new UserPostgresRepo()

      val userPostgresImpl = new UserPostgresImpl(userDb, usersPostgresRepo)

      val requestsPostgresRepo = new RequestPostgresRepo()

      val requestPostgresImpl = new RequestPostgresImpl(userDb, requestsPostgresRepo)

      implicit val moneyRequestActor = context.spawn(UserRequestActor(userPostgresImpl, requestPostgresImpl), "moneyRequestActor")

      implicit val zeebeActor = context.spawn(WorkflowActor(config), "workflowActor")


      val userRoute = new UserRequestRouteWrapper(moneyRequestActor)
      val workflowRoute = new WorkflowRouteWrapper(zeebeActor)
      startHttpServer(concat(userRoute.moneyRequestRoute, workflowRoute.workflowRoute), system = system, config = config)
      Behaviors.empty
    }
    ActorSystem[Nothing](rootBehavior, "main")
  }

}
