package kz.dar.lab.rabbit

import akka.actor.{Actor, Props}
import akka.stream.alpakka.amqp.scaladsl.CommittableReadResult

object RabbitMQManager {
  def props(): Props =
    Props(new RabbitMQManager())
}

class RabbitMQManager extends Actor {
  override def receive: Receive = {
    case message: String => println("RECIEVED MESSAGE")
  }
}
