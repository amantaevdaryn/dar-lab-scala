package kz.dar.lab.rabbit


import akka.actor.ActorSystem
import akka.{Done, NotUsed}
import akka.stream.{ClosedShape, Materializer, Outlet}
import akka.stream.alpakka.amqp.scaladsl.{AmqpSource, CommittableReadResult}
import akka.stream.alpakka.amqp.{AmqpConnectionProvider, Declaration, ExchangeDeclaration, NamedQueueSourceSettings, QueueDeclaration}
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.GraphDSL.Implicits._
import akka.stream.scaladsl.{GraphDSL, RestartSource, RunnableGraph, Sink, Source}
import io.zeebe.client.ZeebeClient
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.JobClient

import scala.jdk.CollectionConverters._
import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration
import scala.util.Random

abstract class RabbitMQConsumer(topicName: String, reconnectionDelay: FiniteDuration, qos: Int)(
  implicit materializer: Materializer,
  connection: AmqpConnectionProvider
) extends Serializers {
  var logger: Logger = LoggerFactory.getLogger(this.getClass)
  protected val routingExchangeDeclaration: ExchangeDeclaration =
    ExchangeDeclaration(topicName, "topic")
      .withDurable(true)
      .withAutoDelete(false)
      .withInternal(false)

  protected def queueDeclaration: QueueDeclaration

  protected def declarations: Seq[Declaration]

  protected val amqpSource: Source[CommittableReadResult, NotUsed] =
    RestartSource.withBackoff(reconnectionDelay, 10 * reconnectionDelay, 0) { () =>
      AmqpSource.committableSource(
        NamedQueueSourceSettings(connection, queueDeclaration.name)
          .withDeclarations(declarations.toList),
        bufferSize = qos
      )
    }

  private def run(busConsumerSink: Sink[CommittableReadResult, Future[Done]]): Future[Done] = {

    val rabbitIncomingSource = RunnableGraph.fromGraph(GraphDSL.create(busConsumerSink) { implicit builder =>
      sink =>
        val amqpOut: Outlet[CommittableReadResult] = builder.add(amqpSource).out
        amqpOut ~> sink.in
        ClosedShape
    })
    rabbitIncomingSource.run()
  }

  def startConsume(client: JobClient, job: ActivatedJob, zeebeClient: ZeebeClient): Future[Done] =
    run(Sink.foreach { message =>
      logger.info(s"CommittableReadResult : ${message.message.bytes.utf8String}")
      client.newCompleteCommand(job.getKey)
        .variables(Map("isValid" -> message.message.bytes.utf8String.toInt).asJava)
        .send().join()
//      zeebeClient.newPublishMessageCommand().
//        messageName("Message_0d16ucc").
//        correlationKey(message.message.bytes.utf8String).send().join()
      message.ack()
    })
}
