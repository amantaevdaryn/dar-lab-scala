package kz.dar.lab.rabbit

import akka.stream.alpakka.amqp.{AmqpConnectionProvider, AmqpCredentials, AmqpDetailsConnectionProvider}

import com.typesafe.config.Config

trait RabbitMQConnector {
  def createAmqpConnection(config: Config): AmqpConnectionProvider = {
    val host = config.getString(s"rabbitmq.connection.amqp-host").split(",")
    val port = config.getString(s"rabbitmq.connection.amqp-port").split(",").map(_.toInt)

    val user = config.getString(s"rabbitmq.connection.name")
    val pass = config.getString(s"rabbitmq.connection.password")
    val vhost = config.getString(s"rabbitmq.connection.virtualHost")

    assert(host.length.equals(port.length), "Hosts length and ports length are not equal")

    val hostsAndPorts = host.zip(port)

    AmqpDetailsConnectionProvider(host.head, port.head)
      .withHostsAndPorts(hostsAndPorts)
      .withCredentials(AmqpCredentials(user, pass))
      .withVirtualHost(vhost)
      .withRequestedHeartbeat(1000)
      .withConnectionTimeout(60000)
      .withHandshakeTimeout(5000)
      .withShutdownTimeout(0)
      .withNetworkRecoveryInterval(10000)
      .withAutomaticRecoveryEnabled(true)
      .withTopologyRecoveryEnabled(true)
  }

}
