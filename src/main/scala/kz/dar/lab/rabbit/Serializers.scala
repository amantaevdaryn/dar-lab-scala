package kz.dar.lab.rabbit
import kz.dar.lab.model.{ErrorInfo, User}
import org.json4s.{ShortTypeHints, jackson}
import org.json4s.jackson.Serialization

trait Serializers {
  implicit val formats = Serialization.formats(
    ShortTypeHints(
      List(
        classOf[User],
        classOf[ErrorInfo]
      )
    )
  )
  implicit val serialization = jackson.Serialization

}
