package kz.dar.lab.rabbit

import akka.stream.Materializer
import akka.stream.alpakka.amqp.{AmqpConnectionProvider, BindingDeclaration, Declaration, QueueDeclaration}

import scala.concurrent.duration.FiniteDuration

class RabbitMQCommandConsumer(
                               topicName: String,
                               queueName: String,
                               endpoint: String,
                               reconnectionDelay: FiniteDuration,
                               qos: Int
                             )(implicit materializer: Materializer, connection: AmqpConnectionProvider)
  extends RabbitMQConsumer(topicName, reconnectionDelay, qos) {

  override protected def queueDeclaration: QueueDeclaration =
    QueueDeclaration(queueName)
      .withDurable(durable = true)
      .withExclusive(exclusive = false)
      .withAutoDelete(autoDelete = false)

  private val keysDeclaration: Seq[Declaration] =
    Seq(BindingDeclaration(queueDeclaration.name, routingExchangeDeclaration.name).withRoutingKey(endpoint))

  override protected def declarations: Seq[Declaration] =
    Seq(queueDeclaration, routingExchangeDeclaration) ++ keysDeclaration
}
