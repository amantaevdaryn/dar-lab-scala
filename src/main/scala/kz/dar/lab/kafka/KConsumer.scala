package kz.dar.lab.kafka

import java.time.Duration
import java.util.{Collections, Properties}

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.config.Config
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.JobClient
import kz.dar.lab.exceptions.{DarLabEndpoint, DarLabErrorCodes, NotFoundException}
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import org.json4s.DefaultFormats
import org.slf4j.{Logger, LoggerFactory}
import org.json4s.jackson.JsonMethods._
import io.circe.generic.auto._
import io.circe.syntax._
import kz.dar.lab.model.{UserRequest}

import scala.collection.immutable.HashMap
import scala.jdk.CollectionConverters._

sealed trait KafkaConsumerCommand
case class KConsumerMessage(topic: String, groupId: String, brokersHost: String, keys: List[String], client: JobClient, job: ActivatedJob) extends KafkaConsumerCommand

object KConsumer extends DarLabEndpoint{
  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  implicit val formats: DefaultFormats.type = DefaultFormats
  def apply()(implicit config: Config): Behavior[KafkaConsumerCommand] = Behaviors
    .setup {
      ctx =>
        Behaviors.receiveMessage {
          case KConsumerMessage(topic, groupId, brokersHost, keys, client: JobClient, job: ActivatedJob) =>
            try {
              val props = configuration(groupId, brokersHost)
              val consumer = new KafkaConsumer[String, String](props)
              consumer.subscribe(Collections.singletonList(topic))
              while (true) {
                Thread.sleep(5000)
                val records: ConsumerRecords[String, String] = consumer.poll(Duration.ofMillis(100000))
                records.asScala.foreach(record => {
                  logger.info(s"record of consumer topic: $topic with key: ${keys.toString()} is: " + record)
                  val resVal = parse(record.value()).extract[Map[String, AnyRef]]
                  val userVal: HashMap[String, AnyVal] = resVal("user").asInstanceOf[HashMap[String, AnyVal]]
                  val user: UserRequest = new UserRequest(userVal("idNumber").asInstanceOf[String], userVal("firstName").asInstanceOf[String],
                    userVal("lastName").asInstanceOf[String], userVal("age").asInstanceOf[Number].intValue(), userVal("workplace").asInstanceOf[String], userVal("closingDate").asInstanceOf[String],
                    userVal("status").asInstanceOf[String])
                  client.newCompleteCommand(job.getKey)
                    .variables(Map("request" ->user.asJson.noSpaces, "isExist" -> 0).asJava)
                    .send().join()

                })
              }
              Behaviors.same
            } catch {
              case exception: Exception =>
                logger.error(s"Exception while consuming message: ${exception.getMessage}")
                throw new RuntimeException(NotFoundException(DarLabErrorCodes.KAFKA_SERVER_ERROR(errorSeries, errorSystem), Some(exception.getMessage)))
                Behaviors.stopped
            }
        }
    }

  private def configuration(groupId: String, brokersHost: String)(implicit config: Config): Properties
  = {
    val props = new Properties()
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokersHost)
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId)
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
    props
  }

}
