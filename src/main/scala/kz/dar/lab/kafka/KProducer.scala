package kz.dar.lab.kafka

import java.util
import java.util.Properties

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import org.apache.kafka.clients.producer._
import com.typesafe.config.Config
import io.zeebe.client.api.response.ActivatedJob
import io.zeebe.client.api.worker.JobClient
import kz.dar.lab.exceptions.{DarLabEndpoint, DarLabErrorCodes, DarLabException, NotFoundException}
import org.apache.kafka.clients.admin.{AdminClient, AdminClientConfig, NewTopic}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.json4s.{DefaultFormats, Formats}
import org.json4s.JsonAST.JValue
import org.json4s.native.Serialization.write
import org.slf4j.{Logger, LoggerFactory}

sealed trait KafkaProducerCommand
case class KProduceMessage(zeebeTask: Option[Map[String, Any]], groupId: String)
  extends KafkaProducerCommand

object KProducer extends DarLabEndpoint{
  val logger: Logger = LoggerFactory.getLogger(this.getClass)
  implicit val formats: DefaultFormats.type = DefaultFormats


  val callback = new Callback {
    override def onCompletion(metadata: RecordMetadata, exception: Exception)
    : Unit = {
      Option(exception) match {
        case Some(err) => println(s"Failed to produce in kafka $err")
        case None => println(s"Produced record at $metadata")
      }
    }
  }

  def apply(topicName: String, brokersHost: String)
  : Behavior[KafkaProducerCommand] = Behaviors
    .setup {
      ctx =>
        try {
          val props = configuration(brokersHost)
          val producer = new KafkaProducer[String, String](props)
          Behaviors.receiveMessage {
            case KProduceMessage(zeebeTask, groupId) =>
              val record = new ProducerRecord[String, String](topicName,
                groupId, write(zeebeTask))
              logger.info(s"Record in kafka producer: $record")
              producer.send(record, callback)
              producer.flush()
              producer.close()
              Behaviors.same
          }
        } catch {
          case exception: Exception =>
            logger.error(s"Error while producing kafka message: exception=${exception.getMessage}")
            Behaviors.stopped
        }
    }

  def configuration(brokersHost: String): Properties = {
    val properties = new Properties
    properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, brokersHost)
    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokersHost)
    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
    properties
  }

}
