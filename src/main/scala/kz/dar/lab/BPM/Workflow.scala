package kz.dar.lab.BPM

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.event.slf4j.Logger
import io.zeebe.client.ZeebeClient
import io.zeebe.client.api.response.{ActivatedJob, DeploymentEvent, WorkflowInstanceEvent}
import io.zeebe.client.api.worker.{JobClient, JobHandler}
import kz.dar.lab.actors.WorkflowActor.{IssuingMoney, RecordProcess, Reject, ValidateProcess}
import kz.dar.lab.actors.{UserRequestActor, WorkflowActor}

import scala.concurrent.{ExecutionContext, Future}
import kz.dar.lab.model.UserRequest


case class Workflow(workflowActor: ActorRef[WorkflowActor.Command])(implicit val system: ActorSystem[_], executionContext: ExecutionContext) {

  val logger = Logger("Root")


  class RecordProcess(user: UserRequest, client: ZeebeClient) extends JobHandler {
    override def handle(client: JobClient, job: ActivatedJob): Unit = {
      workflowActor ! RecordProcess(user.idNumber, user, client, job)
    }
  }

  class ValidateProcess(zeebeClient: ZeebeClient) extends JobHandler {
    override def handle(client: JobClient, job: ActivatedJob): Unit = {
      workflowActor ! ValidateProcess(client, job, zeebeClient)
    }
  }

  class IssuingMoney(client: ZeebeClient) extends JobHandler {
    override def handle(client: JobClient, job: ActivatedJob): Unit = {
      workflowActor ! IssuingMoney(client, job)

    }
  }

  class Reject(client: ZeebeClient) extends JobHandler {
    override def handle(client: JobClient, job: ActivatedJob): Unit = {
      workflowActor ! Reject(client, job)
    }
  }

  def startWorkflow(user: UserRequest): Unit = {
    val zeebeClient: ZeebeClient =
      ZeebeClient.newClientBuilder()
        .brokerContactPoint("localhost:26500")
        .usePlaintext()
        .build()

    val deployment: DeploymentEvent = zeebeClient.newDeployCommand()
      .addResourceFromClasspath("diagram_3.bpmn")
      .send()
      .join()
    val version: Int = deployment.getWorkflows().get(0).getVersion
    logger.info("Workflow deployed. Version: " + version)

    val wfInstance: WorkflowInstanceEvent = zeebeClient.newCreateInstanceCommand()
      .bpmnProcessId("Process")
      .latestVersion()
      .send()
      .join()
    val workflowInstanceKey: Long = wfInstance.getWorkflowInstanceKey()
    logger.info("Workflow instance created. Key: " + workflowInstanceKey)


    zeebeClient.newWorker().jobType("record-process")
      .handler {
        new RecordProcess(user, zeebeClient)
      }.open()
    zeebeClient.newWorker().jobType("validation-process")
      .handler {
        new ValidateProcess(zeebeClient)
      }.open()
    zeebeClient.newWorker().jobType("issuing-process")
      .handler {
        new IssuingMoney(zeebeClient)
      }.open()

    zeebeClient.newWorker().jobType("reject-process")
      .handler {
        new Reject(zeebeClient)
      }.open()


  }

}
