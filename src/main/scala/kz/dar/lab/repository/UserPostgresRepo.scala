package kz.dar.lab.repository

import kz.dar.lab.model.User
import slick.jdbc.PostgresProfile.api._

import scala.language.postfixOps


class UserPostgresRepo {

  class Users(tag: Tag) extends Table[User](tag, "users") {
    lazy val requestTableQuery = new RequestPostgresRepo().requestTableQuery

    def idNumber = column[String]("idNumber")

    def firstName = column[String]("firstName")

    def lastName = column[String]("lastName")

    def age = column[Int]("age")

    def requestId = column[String]("requestId")

    def * = (idNumber, firstName, lastName, age, requestId) <> (User tupled, User unapply (_))

    def request = foreignKey("REQUEST", requestId, requestTableQuery)(_.id, onUpdate = ForeignKeyAction.Restrict, onDelete = ForeignKeyAction.Cascade)

  }

  val userTableQuery = TableQuery[Users]

  def getById(idNumber: String) = {
    userTableQuery.filter(_.idNumber === idNumber).result.headOption
  }


  def insert(user: User) = {
    userTableQuery += user
  }

  def update(user: User) = {
    userTableQuery.filter(_.idNumber === user.idNumber).update(user)
  }

  def delete(idNumber: String) = {
    userTableQuery.filter(_.idNumber === idNumber).delete
  }


}

