package kz.dar.lab.repository

import kz.dar.lab.model.MoneyRequest
import slick.jdbc.PostgresProfile.api._

class RequestPostgresImpl(db: Database, requestPostgresRepo: RequestPostgresRepo) {
  def insertRequest(moneyRequest: MoneyRequest) = {
    val insert = requestPostgresRepo.insert(moneyRequest)
    db.run(insert)
  }

  def getUserById(id: String) = {
    val get = requestPostgresRepo.getById(id)
    db.run(get)
  }

  def update(moneyRequest: MoneyRequest) = {
    val update = requestPostgresRepo.update(moneyRequest)
    db.run(update)
  }

  def delete(id: String) = {
    val delete = requestPostgresRepo.delete(id)
    db.run(delete)

  }

}
