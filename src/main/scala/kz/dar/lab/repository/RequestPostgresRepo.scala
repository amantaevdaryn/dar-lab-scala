package kz.dar.lab.repository

import kz.dar.lab.model.MoneyRequest
import slick.jdbc.PostgresProfile.api._

import scala.language.postfixOps


class RequestPostgresRepo {
  val requestTableQuery = TableQuery[MoneyRequests]


  class MoneyRequests(tag: Tag) extends Table[MoneyRequest](tag, "requests") {
    def id = column[String]("id")

    def workplace = column[String]("workplace")

    def closingDate = column[String]("closingDate")

    def status = column[String]("status")

    def * = (id, workplace, closingDate, status) <> (MoneyRequest tupled, MoneyRequest unapply (_))
  }

  def getById(id: String) = {
    requestTableQuery.filter(_.id === id).result.headOption
  }

  def insert(moneyRequest: MoneyRequest) = {
    requestTableQuery += moneyRequest
  }

  def update(moneyRequest: MoneyRequest) = {
    requestTableQuery.filter(_.id === moneyRequest.id).update(moneyRequest)
  }

  def delete(id: String) = {
    requestTableQuery.filter(_.id === id).delete
  }

}
