package kz.dar.lab.repository

import kz.dar.lab.model.User
import slick.jdbc.PostgresProfile.api._

class UserPostgresImpl(db: Database, userPostgresRepo: UserPostgresRepo) {
  def insertUser(user: User) = {
    val insert = userPostgresRepo.insert(user)
    db.run(insert)
  }

  def getUserById(idNumber: String) = {
    val get = userPostgresRepo.getById(idNumber)
    db.run(get)

  }

  def update(user: User) = {
    val update = userPostgresRepo.update(user)
    db.run(update)
  }

  def delete(idNumber: String) = {
    val delete = userPostgresRepo.delete(idNumber)
    db.run(delete)

  }

}
