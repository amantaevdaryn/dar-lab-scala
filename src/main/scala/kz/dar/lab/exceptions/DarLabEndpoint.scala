package kz.dar.lab.exceptions

trait DarLabEndpoint {
    val errorSystem = DarLabErrorSystem
    val errorSeries = DarLabErrorSeries.SERVICE_DAR_LAB
}
