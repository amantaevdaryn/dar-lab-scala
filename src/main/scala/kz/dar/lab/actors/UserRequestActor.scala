package kz.dar.lab.actors

import java.util.UUID.randomUUID

import akka.actor.Props
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.stream.Materializer
import akka.stream.alpakka.amqp.AmqpConnectionProvider

import scala.concurrent.{ExecutionContext, Promise}
import kz.dar.lab.actors.UserRequestActor.{Command, CreateUserRequest, DeleteUserRequest, GetUserRequest, UpdateUserRequest}
import kz.dar.lab.kafka.{KProduceMessage, KProducer, KafkaProducerCommand}
import kz.dar.lab.model.{ErrorInfo, MoneyRequest, User}
import kz.dar.lab.rabbit.{AmqpProducerCommand, ProduceMessage, RabbitMQManager, RabbitMQProducer}
import kz.dar.lab.repository.{RequestPostgresImpl, UserPostgresImpl}

import scala.concurrent.duration._
import scala.util.{Failure, Success}


object UserRequestActor {

  sealed trait Command

  case class CreateUserRequest(idNumber: String, firstName: String, lastName: String,
                               age: Int, workplace: String, closingDate: String,
                               status: String, replyTo: ActorRef[Either[ErrorInfo, User]]) extends Command

  case class GetUserRequest(idNumber: String, replyTo: ActorRef[Either[ErrorInfo, Option[User]]]) extends Command

  case class DeleteUserRequest(idNumber: String, replyTo: ActorRef[Either[ErrorInfo, User]]) extends Command

  case class UpdateUserRequest(idNumber: String, firstName: String, lastName: String,
                               age: Int, workplace: String, closingDate: String, status: String,
                               replyTo: ActorRef[Either[ErrorInfo, User]]) extends Command

  def apply(postgresUserImpl: UserPostgresImpl, postgresRequestImpl: RequestPostgresImpl)(
    implicit executionContext: ExecutionContext,
    materializer: Materializer,
    connection: AmqpConnectionProvider): Behavior[Command] =
    Behaviors.setup(context => new UserRequestActor(postgresUserImpl, postgresRequestImpl, context).recive())

}


class UserRequestActor(postgresUserImpl: UserPostgresImpl, postgresRequestImpl: RequestPostgresImpl,
                       context: ActorContext[UserRequestActor.Command])(
                        implicit executionContext: ExecutionContext,
                        materializer: Materializer,
                        connection: AmqpConnectionProvider) {

  def recive(): Behavior[Command] = {
    Behaviors.receiveMessage {

      case getUser: GetUserRequest =>
        postgresUserImpl.getUserById(getUser.idNumber).onComplete {
          case Success(value) =>
            getUser.replyTo ! Right(value)
          case Failure(exception) =>
            getUser.replyTo ! Left(ErrorInfo(500, exception.getMessage))
        }

        Behaviors.same

      case createUser: CreateUserRequest =>


        val request = MoneyRequest(randomUUID().toString, createUser.workplace, createUser.closingDate, createUser.status)
        val user = User(createUser.idNumber, createUser.firstName, createUser.lastName, createUser.age, request.id)
        val createUserFuture = postgresUserImpl.insertUser(user)
        val createRequestFuture = postgresRequestImpl.insertRequest(request)
        val result = for {
          userFuture <- createUserFuture
          _ <- createRequestFuture
        } yield userFuture

        result.onComplete {
          case Success(value) =>
            createUser.replyTo ! Right(user)
          case Failure(exception) =>
            createUser.replyTo ! Left(ErrorInfo(500, exception.getMessage))
        }
        Behaviors.same

      case updateUser: UpdateUserRequest =>
        val userFuture = postgresUserImpl.getUserById(updateUser.idNumber)
        val promiseUser = Promise[Option[User]]()
        promiseUser completeWith (userFuture)
        promiseUser.future.foreach {
          case Some(currentUser) =>
            val request = MoneyRequest(currentUser.requestId, updateUser.workplace, updateUser.closingDate, updateUser.status)
            val user = User(updateUser.idNumber, updateUser.firstName, updateUser.lastName, updateUser.age, currentUser.requestId)
            val updateUserFuture = postgresUserImpl.update(user)
            val updateRequestFuture = postgresRequestImpl.update(request)
            val result = for {
              userFuture <- updateUserFuture
              _ <- updateRequestFuture
            } yield userFuture
            result.onComplete {
              case Success(value) =>
                updateUser.replyTo ! Right(user)
              case Failure(exception) =>
                updateUser.replyTo ! Left(ErrorInfo(500, exception.getMessage))
            }
          case None => updateUser.replyTo ! Left(ErrorInfo(500, "User not found"))
        }

        Behaviors.same

      case deleteUserRequest: DeleteUserRequest =>
        val userFuture = postgresUserImpl.getUserById(deleteUserRequest.idNumber)
        val promiseUser = Promise[Option[User]]()
        promiseUser completeWith (userFuture)
        promiseUser.future.foreach {
          case Some(currentUser) =>
            val deleteUserFuture = postgresUserImpl.delete(deleteUserRequest.idNumber)
            val deleteRequestFuture = postgresRequestImpl.delete(currentUser.requestId)
            val result = for {
              userFuture <- deleteUserFuture
              _ <- deleteRequestFuture
            } yield userFuture
            result.onComplete {
              case Success(value) =>
                deleteUserRequest.replyTo ! Right(currentUser)
              case Failure(exception) =>
                deleteUserRequest.replyTo ! Left(ErrorInfo(500, exception.getMessage))
            }
          case None => deleteUserRequest.replyTo ! Left(ErrorInfo(500, "User not found"))
        }

        Behaviors.same

    }
  }
}
