package kz.dar.lab.actors

import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.event.slf4j.Logger
import io.zeebe.client.api.response.ActivatedJob
import kz.dar.lab.actors.WorkflowActor.{Command, IssuingMoney, RecordProcess, Reject, ValidateProcess}
import kz.dar.lab.model.{ErrorInfo, User, UserRequest}
import kz.dar.lab.services.ClientService
import akka.http.scaladsl.model.HttpResponse
import akka.stream.Materializer
import akka.stream.alpakka.amqp.AmqpConnectionProvider
import com.typesafe.config.Config
import kz.dar.lab.rabbit.{AmqpProducerCommand, ProduceMessage, RabbitMQCommandConsumer, RabbitMQProducer}
import io.circe.parser.decode
import io.circe.generic.auto._
import io.zeebe.client.ZeebeClient
import io.zeebe.client.api.worker.JobClient
import kz.dar.lab.kafka.{KConsumer, KConsumerMessage, KProduceMessage, KProducer, KafkaConsumerCommand, KafkaProducerCommand}
import scala.concurrent.{ExecutionContext, Promise}
import scala.util.{Failure, Success}
import scala.concurrent.duration._


object WorkflowActor {


  sealed trait Command

  case class CreateUserRequest(idNumber: String, firstName: String, lastName: String,
                               age: Int, workplace: String, closingDate: String,
                               status: String, replyTo: ActorRef[Either[ErrorInfo, User]]) extends Command

  case class RecordProcess(idNumber: String, userRequest: UserRequest, client: JobClient, job: ActivatedJob) extends Command

  case class ValidateProcess(client: JobClient, job: ActivatedJob, zeebeClient: ZeebeClient) extends Command

  case class IssuingMoney(client: JobClient, job: ActivatedJob) extends Command

  case class Reject(client: JobClient, job: ActivatedJob) extends Command

  def apply(config: Config)(implicit executionContext: ExecutionContext, materializer: Materializer,
                            connection: AmqpConnectionProvider): Behavior[Command] =
    Behaviors.setup(context => new WorkflowActor(context, config).receive())

}

class WorkflowActor(context: ActorContext[WorkflowActor.Command], config: Config)(
  implicit executionContext: ExecutionContext,
  materializer: Materializer,
  connection: AmqpConnectionProvider) {
  val logger = Logger("Root")
  implicit val system = context.system
  val userService: ClientService = ClientService()


  def receive(): Behavior[Command] = {
    Behaviors.receiveMessage {
      case recordProcess: RecordProcess =>
        val user = userService.getUserRequest(recordProcess.idNumber)
        val promiseUser = Promise[HttpResponse]()

        val produceMessage = KProduceMessage(Some(Map("user" -> recordProcess.userRequest)), "dar-lab")
        val producer: ActorSystem[KafkaProducerCommand] = ActorSystem(
          KProducer.apply(config.getString("kafka.topicName"), config.getString("kafka.brokerHost")),
          "producer-kafka")
        producer ! produceMessage


        promiseUser completeWith (user)

        promiseUser.future.foreach(x =>
          if (decode[User](x.entity.toString) != None) {
            userService.postUserRequest(recordProcess.userRequest).onComplete {
              case Success(value) =>
                val consumerMessage = KConsumerMessage(config.getString("kafka.topicName"),
                  config.getString("kafka.dar-lab-group"), config.getString("kafka.brokerHost"),
                  List(config.getString("kafka.key")), recordProcess.client, recordProcess.job)
                val consumer: ActorSystem[KafkaConsumerCommand] =
                  ActorSystem(KConsumer.apply()(config), "consumer-kafka")
                consumer ! consumerMessage
              case Failure(exception) =>
                logger.info(exception.toString)
            }
          }
        )
        Behaviors.same

      case validateProcess: ValidateProcess =>


        var variables = validateProcess.job.getVariablesAsMap
        val userRequest = decode[UserRequest](variables.get("request").toString).getOrElse(null)

        val produceUser = ProduceMessage(
          userRequest,
          "request.dar-lab.create-user",
          context.self.path.toStringWithoutAddress,
          Some("reply.dar-lab.user"),
          Map()
        )
        val producer: ActorSystem[AmqpProducerCommand] =
          ActorSystem(RabbitMQProducer.apply(10.seconds, "X:routing.topic", "topic"), "producer")

        /*
            simulate validation of user with thread sleep 10000 ms and send to rabbitMQ
         */
        producer ! produceUser


        userRequest.status = "VALIDATED"

        val amqpCommandConsumer = new RabbitMQCommandConsumer(
          config.getString("rabbitmq.consumers.exchange"),
          config.getString("rabbitmq.consumers.queueName"),
          config.getString("rabbitmq.consumers.routingKey"),
          2.minutes,
          10
        )

        /*
          listen until answer is published and complete the process
        */

        userService.putUserRequest(userRequest)
          .onComplete {
            case Success(value) =>
              amqpCommandConsumer.startConsume(validateProcess.client, validateProcess.job, validateProcess.zeebeClient)
            case Failure(exception) =>
              logger.info(exception.toString)
          }


        Behaviors.same


      case issuingMoney: IssuingMoney =>

        var variables = issuingMoney.job.getVariablesAsMap()
        val userRequest = decode[UserRequest](variables.get("request").toString).getOrElse(null)
        userRequest.status = "ACCEPTED"
        userService.putUserRequest(userRequest)
          .onComplete {
            case Success(value) =>
              issuingMoney.client.newCompleteCommand(issuingMoney.job.getKey)
                .send().join()
            case Failure(exception) =>
              logger.info(exception.toString)
          }
        Behaviors.same


      case reject: Reject =>
        var variables = reject.job.getVariablesAsMap()
        val userRequest = decode[UserRequest](variables.get("request").toString).getOrElse(null)
        userRequest.status = "REJECTED"
        userService.putUserRequest(userRequest)
          .onComplete {
            case Success(value) =>
              reject.client.newCompleteCommand(reject.job.getKey)
                .send().join()
            case Failure(exception) =>
              logger.info(exception.toString)
          }
        Behaviors.same
    }

  }
}
