package kz.dar.lab.model

import cats.syntax.functor._
import io.circe.bson.BsonCodecInstances
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import slick.lifted.Tag
import slick.model.Table

sealed trait Domain

case class ErrorInfo(errorCode: Int, message: String)

case class MoneyRequest(id: String, workplace: String, closingDate: String, status: String) extends Domain

case class User(idNumber: String, firstName: String, lastName: String, age: Int, requestId: String) extends Domain

case class UserRequest(idNumber: String, firstName: String, lastName: String,
                       age: Int, workplace: String, closingDate: String, var status: String) extends Domain

trait MainCodec extends BsonCodecInstances {
  implicit val encodeDomainEntity: Encoder[Domain] = Encoder.instance {
    case e: User => e.asJson
    case e: MoneyRequest => e.asJson
    case e: UserRequest => e.asJson
  }
  implicit val decodeDomainEntity: Decoder[Domain] =
    List[Decoder[Domain]](
      Decoder[Domain].widen
    ).reduceLeft(_.or(_))
}


