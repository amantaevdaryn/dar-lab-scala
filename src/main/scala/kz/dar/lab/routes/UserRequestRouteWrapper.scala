package kz.dar.lab.routes

import akka.http.scaladsl.server.Directives.concat
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives._

import kz.dar.lab.actors.UserRequestActor


class UserRequestRouteWrapper(val moneyRequestActor: ActorRef[UserRequestActor.Command])(implicit val system: ActorSystem[_])
  extends UserRequestRoute {
  val moneyRequestRoute = pathPrefix("user") {
    concat(
      route
    )
  }
}
