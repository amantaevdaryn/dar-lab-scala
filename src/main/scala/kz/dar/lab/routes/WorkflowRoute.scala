package kz.dar.lab.routes
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import kz.dar.lab.BPM.Workflow
import kz.dar.lab.actors.WorkflowActor
import kz.dar.lab.model.UserRequest
import io.circe.generic.auto._
import io.circe.syntax._
import spray.json.DefaultJsonProtocol._

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol.jsonFormat7
trait WorkflowRoute {

  val workflowActor: ActorRef[WorkflowActor.Command]


  implicit val system: ActorSystem[_]

  implicit val timeout: Timeout = Timeout.create(system.settings.config.getDuration("http-duration"))

  implicit val executionContext = system.executionContext

  implicit val requestBodyFormatter = jsonFormat7(UserRequest)

  val route: Route = {
    post {
      entity(as[UserRequest]) { body =>
        Workflow(workflowActor).startWorkflow(body)
        complete(HttpEntity(ContentTypes.`application/json`, s"completed"))
      }
    }
  }
}
