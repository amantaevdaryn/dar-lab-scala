package kz.dar.lab.routes

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives.{concat, pathPrefix}
import kz.dar.lab.actors.WorkflowActor

class WorkflowRouteWrapper(val workflowActor: ActorRef[WorkflowActor.Command])(implicit val system: ActorSystem[_]) extends WorkflowRoute {
  val workflowRoute = pathPrefix("workflow") {
    concat(
      route
    )
  }

}
