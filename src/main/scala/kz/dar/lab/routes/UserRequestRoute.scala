package kz.dar.lab.routes


import akka.http.scaladsl.server.Route
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import scala.concurrent.Future
import scala.util.{Failure, Success}

import spray.json.DefaultJsonProtocol._

import io.circe.generic.auto._
import io.circe.syntax._


import kz.dar.lab.actors.UserRequestActor.{CreateUserRequest, DeleteUserRequest, GetUserRequest, UpdateUserRequest}
import kz.dar.lab.model.{ErrorInfo, User, UserRequest}
import kz.dar.lab.actors.UserRequestActor


trait UserRequestRoute {

  val moneyRequestActor: ActorRef[UserRequestActor.Command]

  implicit val system: ActorSystem[_]

  implicit val timeout: Timeout = Timeout.create(system.settings.config.getDuration("http-duration"))

  implicit val executionContext = system.executionContext

  implicit val requestBodyFormatter = jsonFormat7(UserRequest)

  def getMoneyRequest(idNumber: String): Future[Either[ErrorInfo, Option[User]]] = {
    moneyRequestActor.ask(GetUserRequest(idNumber, _))
  }

  def createMoneyRequest(body: UserRequest): Future[Either[ErrorInfo, User]] = {
    moneyRequestActor.ask(CreateUserRequest(body.idNumber, body.firstName, body.lastName, body.age, body.workplace,
      body.closingDate, "CREATED", _))
  }

  def updateMoneyRequest(body: UserRequest): Future[Either[ErrorInfo, User]] = {
    moneyRequestActor.ask(UpdateUserRequest(body.idNumber, body.firstName, body.lastName, body.age, body.workplace,
      body.closingDate, body.status, _))
  }

  def deleteMoneyRequest(idNumber: String): Future[Either[ErrorInfo, User]] = {
    moneyRequestActor.ask(DeleteUserRequest(idNumber, _))
  }


  val route: Route = {
    concat(
      get {
        path(Segment) { idNumber: String =>
          onComplete(getMoneyRequest(idNumber)) {
            case Success(value) =>
              complete(HttpEntity(ContentTypes.`application/json`, value.asJson.dropNullValues.noSpaces))
            case Failure(exception) =>
              complete(HttpEntity(ContentTypes.`application/json`, exception.toString))
          }
        }
      },
      post {
        entity(as[UserRequest]) { body =>
          onComplete(createMoneyRequest(body)) {
            case Success(value) =>
              complete(HttpEntity(ContentTypes.`application/json`, value.asJson.noSpaces))
            case Failure(exception) =>
              complete(HttpEntity(ContentTypes.`application/json`, exception.toString))
          }
        }
      },
      put {
        entity(as[UserRequest]) { body =>
          onComplete(updateMoneyRequest(body)) {
            case Success(value) =>
              complete(HttpEntity(ContentTypes.`application/json`, value.asJson.noSpaces))
            case Failure(exception) =>
              complete(HttpEntity(ContentTypes.`application/json`, exception.toString))
          }
        }
      },
      delete {
        path(Segment) { idNumber: String =>
          onComplete(deleteMoneyRequest(idNumber)) {
            case Success(value) =>
              complete(HttpEntity(ContentTypes.`application/json`, value.asJson.noSpaces))
            case Failure(exception) =>
              complete(HttpEntity(ContentTypes.`application/json`, exception.toString))
          }

        }
      }
    )
  }
}
